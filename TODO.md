# Proposed Changes

## Additional Features

- **Implement interface for Livy class.** This would be vital for helping to preserve backward compatibility with future versions.
- Add capability for meta fields to autosave? _This may not be possible_
- Block "templates" - The ability to define, and then add, a pre-selected set of blocks to a page (empty blocks). _this looks like it would be a javascript-based solution_
    - Initial implementation will be defined in the theme
    - Possible expansion that allows users to define templates through a GUI

## Bugs

## Fixed 

- **Issue:** Removing post type support for `editor` from Composer post types caused an issues where a post had to be updated twice in order to trigger `Livy\build()` and generate content.
    **Solution:** `Livy\build()` was being triggered on the `save_post` action, which fires when post content is changed. Since I was changing post metadata, not content, it wasn't (I think) firing. I changed it so that `Livy\build()` so that it runs on the `updated_postmeta` action instead, which makes more sense: Since the post is constructed from metadata, it should be regenerated whenever that data changes.
- **Issue:** Password protection did not work correctly--password protecting a post did nothing except add "Protected" to the title.
    **Solution:** The password form was being adding during `Livy\build()` which didn't really make sense because we want returned, not dumped into the content. I moved it to `Livy\testContent()`, so it's now run just before the hash is checked and does not modify the content. 

## Misc

- Add a special user role that can directly observe the editor? Not sure what value this would add, though

## v0.2.0

- Evaluate and possibly re-factor methods
- Evaluate and possibly re-factor block/template flow

### Maybe

- Refactor string functions to work with UTF-8