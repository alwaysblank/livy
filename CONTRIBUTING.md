## Conventions

I've established some rough conventions to help keep things unified. I'll attempt to codify them here.

### CSS

#### Selectors

All CSS selectors (class/id) that apply formatting should be prepended with `__livy__`. This applies to both administrative and frontend selectors. Additional namespace sections should be separated with more double-underscores, i.e. `__livy__picture__imgwrap`. Multi-word sections can be separated with dashes, i.e. `.__livy__admin__hide-label`. Modifiers ffor a section are separated with a single underscore, i.e. `__livy-pad_triple`. 

There are a limited number of selectors which do not use the `__livy__` prefix, but these are generally "switch" selectors--classes that are used by CSS or Javascript to identify elements. They should not be used on their own to apply stypes--only in conjunction with other selectors.


### PHP

#### Method/Property Names

Method and property names should be written in camelCase with the first letter in lowercase. If a segment other than the first would normally be written all lowercase (i.e. "url") it should be written entirely in uppercase, i.e. `setURL`.

#### Loops

Whenever possible, loops should be written in `if ( condition ) : do_stuff(); endif;` style (i.e. without brackets).