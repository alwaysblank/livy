var gulp = require('gulp');
var postcss = require('gulp-postcss');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var cssnext = require('postcss-cssnext');
var uglify = require('gulp-uglify');  
var cssnano = require('gulp-cssnano');
var gutil = require('gulp-util');

var watch = {
    css: ['./src/blocks/**/*.css'],
    js: ['./src/js/**/*.js','./bower_components/Countable/Countable.js']
}

var assetsCSS = [
    {
        filename: 'admin.css',
        paths: ['./src/blocks/base/*.css']
    },
    {
        filename: 'front.css',
        paths: ['./src/blocks/**/*.css','!admin.css']
    }
];
var assetsJS = [    
    {
        filename: 'admin.js',
        paths: ['./src/js/**/*.js','./bower_components/Countable/Countable.js', './bower_components/arrive/src/arrive.js']
    },
    {
        filename: 'front.js',
        paths: []
    }
]

gulp.task('css', function () {
    var processors = [
        cssnext(),
    ];
    return assetsCSS.forEach(function(obj){
        return gulp.src(obj.paths)
            .pipe(concat(obj.filename))
            .pipe(postcss(processors))
            .pipe(cssnano())
            .pipe(gulp.dest('./dist/'));
    });    
});

gulp.task('js', function () {
    return assetsJS.forEach(function(obj){
        return gulp.src(obj.paths)
            .pipe(concat(obj.filename))
            .pipe(uglify().on('error', gutil.log))
            .pipe(gulp.dest('./dist/'));
        });    
});

gulp.task('watch', ['js', 'css'], function() {
  gulp.watch(watch.css, ['css']);
  gulp.watch(watch.js, ['js']);
});

gulp.task('default', ['js', 'css'], function() {});