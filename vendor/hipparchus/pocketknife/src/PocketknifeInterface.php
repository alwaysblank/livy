<?php 
namespace Hipparchus;

if (!interface_exists('PocketknifeInterface')) {

    interface PocketknifeInterface
    {
        /**
         * Checks a string to see if it matches a regex. Used to perform simple validation.
         * 
         * If no regex is supplied, it uses its own hopefully sensible default.
         * 
         * @param type $string 
         * @param string $regex 
         * @return string|bool
         */
        public static function safeString($string, $regex);

        /**
         * Cleans a string in accordance with the regex that's passed to it.
         * 
         * If no regex is supplied, it uses an internal default.
         * 
         * @param string $string 
         * @param string $regex 
         * @return string
         */
        public static function cleanString($string, $replace, $regex);

        /**
         * When passed an object or array, this returns a nicely formatted display
         * to help determine what's inside something.
         * 
         * When bool false is passed as $long, it uses var_dump. When bool true
         * is passed, it uses print_r.
         * 
         * @param object|array $item 
         * @param bool $long 
         * @return string
         */
        public static function inspect($item, $long);
    }

}