<?php 
namespace Hipparchus;

require_once( 'PocketknifeInterface.php' );

if (interface_exists('Hipparchus\PocketknifeInterface') && !class_exists('Util')) {
    
    class Pocketknife implements PocketknifeInterface
    {
        const CLEAN = "/[^[:alnum:]_]/u"; 
        const SAFE = "/^\w+$/";

        public static function safeString($string, $regex = self::SAFE)
        {
            if (preg_match($regex, $string)) :
                 return $string; 
            else:
                 return false;
            endif;
        }

        /**
         * Shorter version for lazy programmers.
         * @see safeString
         */
        public static function sf($string, $regex = self::SAFE)
        {
            return self::safeString($string, $regex);
        }

        public static function cleanString($string, $replace = "", $regex = self::CLEAN)
        {
            if ($replace === null) : $replace = ""; endif;
            if ($regex === null) : $regex = self::CLEAN; endif;
            
            return preg_replace($regex, $replace, $string);
        }

        /**
         * Shorter version for lazy programmers.
         * @see cleanString
         */
        public static function sc($string, $replace = "", $regex = self::CLEAN)
        {
            return self::cleanString($string, $replace, $regex);
        }

        public static function inspect($item, $long = true)
        {
            echo "<pre>";
            if ($long === true) :
                print_r($item);
            elseif ($long === false) :
                var_dump($item);
            endif;
            echo "</pre>";
        }

        /**
         * Shorter version for lazy programmers.
         * @see inspect
         */
        public static function i($item, $long = true)
        {
            self::inspect($item, $long);
        }
    }
}