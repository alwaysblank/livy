<?php
require_once plugin_dir_path(__FILE__) . '/vendor/autoload.php';

use Livy\Cyrus;
use Hipparchus\Pocketknife as Util;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/*
  Plugin Name: Livy
  Plugin URI: n/a
  Description: A content composer plugin for WordPress, to help people build rich pages and posts. Named for the legendary Roman historian.
  Version: 0.0
  Author: Ben Bateman
 */

class Livy
{
    private static $instance = null;
    private $pluginPath;
    private $pluginURL;
    private $textDomain = 'livy'; // Because automated localization is dumb, this must be **manually** added **as a string** to `__()` translation functions.
    private $postTypes = array();
    private $forbiddenPostTypes = ['nav_menu_item', 'revision', 'attachment']; // These are post types we never want to attach a Composer to
    private $blockTypes = ['headline', 'tagline', 'title', 'intro', 'text', 'picture', 'list']; // these are the basic block types Livy comes with
    private $adminUserRoles = ['administrator']; // Roles that have access to Composer settings

    /**
     * Creates or returns an instance of this class.
     */
    public static function getInstance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

        public function safeString( $string ) {
            return Util::safeString($string);
        }

    /**
     * Returns a list of all available post types that the Composer can be attached to.
     * @return array
     */
    public function getPossiblePostTypes()
    {
        $args = array();

        $post_types = get_post_types($args, 'objects');

        foreach ($post_types  as $post_type) {
            if (in_array($post_type->name, $this->forbiddenPostTypes)) :
                    continue;
            endif;

            $name = $post_type->labels->name;
            if (!$name) :
                    $name = $post_type->name;
            endif;
            $this->addPostType($post_type->name, $name);
        }

        return apply_filters('livy_posttype_setup', $this->postTypes);
    }

    /**
     * Return a list post types that the Composer should be attached to.
     * @return array
     */
    public static function getActivePostTypes()
    {
        return apply_filters('livy_posttype_load', carbon_get_theme_option('livy_post_types'));
    }

    /**
     * Adds a post type to the Livy $postTypes property.
     * Throws an exception if the post type is not valid.
     * @param string $postType 
     * @param string $postTypeName 
     * @return void
     */
    public function addPostType($postType, $postTypeName)
    {
        if ($this->safeString($postType) && is_string($postType)) :
                $this->postTypes[$postType] = $postTypeName; else :
                throw new Exception(__("This isn't a valid type name.", 'livy'));
        endif;
    }

    /**
     * Returns a list of available block types.
     * @return array
     */
    public function getPossibleBlockTypes()
    {
        return apply_filters('livy_block_setup', $this->blockTypes);
    }

    /**
     * Returns a list of block types that should appear in the Composer.
     * @return array
     */
    public function getActiveBlockTypes()
    {
        $blocks = carbon_get_theme_option('livy_block_types');
        if (!$blocks) :
            $blocks = $this->blockTypes;
        endif;
        return apply_filters('livy_block_load', $blocks);
    }

    /**
     * Returns an array of all roles it can find, skipping anything 
     * hardcoded into $this->adminUserRoles (currently just the 
     * adiminstrator role).
     * @return array
     */
    public function getPossibleAdminRoles()
    {
        if (!function_exists('get_editable_roles')) {
            require_once ABSPATH.'/wp-admin/includes/user.php';
        }
        $roles = get_editable_roles();
        foreach ($roles as $role => $data) {
            if (in_array($role, $this->adminUserRoles)) : continue;
            endif;
            $list[] = $role;
        }

        return apply_filters('livy_admin_user_setup', $list);
    }

    /**
     * Returns all roles authorized to access the Composer settings.
     * @return array
     */
    public function getActiveAdminRoles()
    {
        $roles = carbon_get_theme_option('livy_user_roles');
        if (!$roles) :
                $return = $this->adminUserRoles; else :
                $return = $this->adminUserRoles + $roles;
        endif;

        return apply_filters('livy_admin_user_load', $return);
    }

    /**
     * Enqueue and register JavaScript files here.
     * @return void
     */
    public function registerScripts()
    {
        if (file_exists($this->getPluginPath().'dist/front.js')) :
            wp_enqueue_script('livy_js', $this->getPluginURL().'dist/front.js', ['select2_js', 'jquery', 'carbon-fields']);
        endif;
    }

    /**
     * Enqueue and register CSS files here.
     * @return void
     */
    public function registerStyles()
    {
        if (file_exists($this->getPluginPath().'dist/front.css')) :
            wp_enqueue_style('livy_css', $this->getPluginURL().'dist/front.css');
        endif;
    }

    /**
     * Enqueue and register JavaScript files for admin here.
     * @return void
     */
    public function registerAdminScripts()
    {
        wp_enqueue_script('select2_js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js', ['jquery']);
        if (file_exists($this->getPluginPath().'dist/admin.js')) :
            wp_enqueue_script('livy_js', $this->getPluginURL().'dist/admin.js', ['select2_js', 'jquery', 'carbon-fields']);
        endif;
    }

    /**
     * Enqueue and register CSS files for admin here.
     * @return void
     */
    public function registerAdminStyles()
    {
        if (file_exists($this->getPluginPath().'dist/admin.css')) :
            wp_enqueue_style('livy_css', $this->getPluginURL().'dist/admin.css');
        endif;
        wp_enqueue_style('select2_css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css');
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     * @return void
     */
    private function __construct()
    {
        $this->pluginPath = plugin_dir_path(__FILE__);
        $this->pluginURL = plugin_dir_url(__FILE__);

        load_plugin_textdomain($this->textDomain, false, $this->pluginPath.'\lang');

        add_action('admin_enqueue_scripts', array($this, 'registerAdminScripts'));
        add_action('admin_enqueue_scripts', array($this, 'registerAdminStyles'));

        add_action('wp_enqueue_scripts', array($this, 'registerScripts'));
        add_action('wp_enqueue_scripts', array($this, 'registerStyles'));

        register_activation_hook(__FILE__, array($this, 'activation'));
        register_deactivation_hook(__FILE__, array($this, 'deactivation'));

        $this->runPlugin();
    }

    /**
     * Returns the URL where the plugin's files are located.
     * @return string
     */
    public function getPluginURL()
    {
        return $this->pluginURL;
    }

    /**
     * Returns the filesystem path where the plugin's files are located.
     * @return string
     */
    public function getPluginPath()
    {
        return $this->pluginPath;
    }

    /**
     * Place code that runs at plugin activation here.
     * @return void
     */
    public function activation()
    {

        // Only set block types if they don't exist
        if (get_option('livy_block_types') === false) :
            update_option('livy_block_types', $this->blockTypes);
        endif;

        // Only set post types if they don't exist
        if (get_option('livy_post_types') === false) :
            update_option('livy_post_types', $this->postTypes);
        endif;
    }

    /**
     * Place code that runs at plugin deactivation here.
     * @return void
     */
    public function deactivation()
    {
        foreach ($this->getPossibleAdminRoles() as $role) {
            $roleObj = get_role($role);
            $roleObj->remove_cap('livy_settings_access');
        }
    }

    /**
     * Retrieves raw meta field data that the post content will be constructed from.
     * 
     * Exposes `livy_build_load_blocks` filter for modifying block content before processing.
     * 
     * @param int $id 
     * @return array
     */
    public function getFieldContent($id)
    {
        return apply_filters('livy_build_load_blocks', carbon_get_post_meta($id, 'livy_blocks', 'complex'));
    }

    /**
     * Runs array data through appropriate template files, then combines and returns that content.
     * 
     * Exploses `livy_build_output` filter to modify returned string before it is returned.
     * 
     * @param array $blocks 
     * @return string
     */
    public function processFieldContent($blocks)
    {
        ob_start();

        if (count($blocks) > 0) :

            reset($blocks);

        while (list($key, $val) = each($blocks)) :

                 if ($override = locate_template('livy-layout-'.ltrim($val['_type'], '_').'.php')) :
                   include $override; else :
                    $template_path = $this->getPluginPath().'src/blocks/'.$val['_type'].'/layout-'.ltrim($val['_type'], '_').'.php';
        if (!file_exists($template_path)) :
                        continue;
        endif;
        include $template_path;
        unset($template_path);
        endif;

        endwhile;

        endif;

        $content = ob_get_clean();

        $content = "<div class='__livy'>$content</div>";
        return apply_filters('livy_build_output', $content);
    }

    /**
     * Updates the post with the passed content, and adds a hash for checking that content in the future.
     * 
     * Exposes `livy_build_update_post_content` action, which runs just before the post is actually updated.
     * 
     * @param int $id 
     * @param string $content 
     * @return int
     */
    public function updatePostContent($id, $content)
    {
        remove_action('updated_postmeta', array($this, 'build'));
        update_post_meta($id, 'content_master_hash', hash('md4', $content));
        add_action('updated_postmeta', array($this, 'build'));

        if (!get_the_title($id)) :
            $title = $id; 
        else :
            $title = get_the_title($id);
        endif;

        $update_array = array(
            'ID' => $id,
            'post_content' => $content,
            'post_title' => $title,
        );

        do_action('livy_build_update_post_content', $id, $content);

        return wp_update_post($update_array, true);
    }

    /**
     * This builds the content of a post from its attached Carbon fields by iterating through the
     * block templates files and generating HTML, which is then inserted into the post object's content.
     * It does this by calling several other functions: `build()` itself contains very little logic.
     * 
     * It will always update the post when it is run, but will also return the content it generates.
     * 
     * This method also exposes the `livy_build` action, which runs as soon as the method is
     * invoked, before it has done anything.
     * 
     * @param string $content 
     * @param int|null $id 
     * @return string
     */
    public function build($content, $id = null)
    {
        do_action('livy_build', $content, $id);

        if (!$id) {
            $id = get_the_id();
        }

        $content = $this->processFieldContent( $this->getFieldContent($id) );

        $update = $this->updatePostContent( $id, $content );

        return $content;        
    }

    /**
     * Compares a hash created when the post was updated to a hash of the content that is passed
     * to it. If they don't match, it recreates the post content and returns that. Also returns the 
     * password form instead of the content if this item is password-protected.
     * 
     * @param type $content 
     * @param int|null $id 
     * @return string
     */
    public function testContent($content, $id = null)
    {
        if ($id === null) :
            $id = get_the_id();
            if (!$id) : return $content; endif;
        endif;

        if (post_password_required()) { 
            return get_the_password_form(); 
        }

        if (hash('md4', $content) == get_post_meta($id, 'content_master_hash', true)) :
            return $content; else:
            return $this->build($content, $id);
        endif;
    }

    /**
     * Give correct roles access to the Composer settings.
     * @return void
     */
    public function grantSettingsAccess()
    {
        foreach ($this->getActiveAdminRoles() as $role) {
            $roleObj = get_role($role);
            $roleObj->add_cap('livy_settings_access');
        }
    }

    /**
     * Hide the content editor on post types where the Composer is used.
     * @return void
     */
    public function removeContentEditor()
    {
        if($this->getActivePostTypes()) :
            foreach ($this->getActivePostTypes() as $type) {
                remove_post_type_support($type, 'editor');
            }
        endif;
    }

    /**
     * Load block definitions.
     * @return void
     */
    public function hookBlocks()
    {
        if (is_admin()) :
         include $this->pluginPath.'src/blocks/blocks.php';
        endif;
    }

     /**
      * When the meta on a post is updated (i.e. Carbon Fields has saved data),
      * call `build` to generate the post content.
      * @return void
      */
     public function hookBuild()
     {
         add_action('updated_postmeta', array($this, 'build'));
     }

     /**
      * Call `testContent` to verify this content is correct.
      * @return void
      */
     public function hookCompare()
     {
         add_filter('the_content', array($this, 'testContent'), 1);
     }

    /**
     * Add our startup actions to the `livy_run` hook and execute them.
     */
    private function runPlugin()
    {
        add_action('livy_run', array($this, 'grantSettingsAccess'));

        add_action('livy_run', array($this, 'removeContentEditor'));

        add_action('livy_run', array($this, 'hookBlocks'), 15);

        add_action('livy_run', array($this, 'hookBuild'), 20);

        add_action('livy_run', array($this, 'hookCompare'), 25);

        do_action('livy_run', $this);

        include $this->getPluginPath().'src/options.php';
    }
}
add_action('init', array('Livy', 'getInstance'), 20);
