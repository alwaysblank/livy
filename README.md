# Livy
### A simple composer for WordPress

## Usage

### Blocks

The content in the Composer is constructed with Blocks. Each Block has a type, i.e. Text, Tagline, Picture, etc. You can create and delete blocks as you like, as well as move them around.

### Settings

Settings for the Composer can be found in the Composer menu item on the Dashboard.

#### Block Types

This allows you to set which Blocks will appear in the Composer. When it is first activated, Composer will turn on its default set of blocks, but after that your settings will be saved, even if you deactivate the plugin. 

> **Note:** Blocks can be forced to active status through the use of the `livy_block_load` filter. If blocks have been added using this filter, they won't appear in the settings page.

#### Post Types

This allows you to select which post types will have the Composer activated for them. It should work with just about any custom post type, in accition to mundane posts and pages, but since I can't predict how people might configure their custom post types you're kind of on your own. 

> **Note:** Currently, additional post type must be added options (with `livy_posttype_setup`) or forced (with `livy_posttype_load`). Post types that are forced will not appear on the settings page.

-

## Filters/Actions

### Filters

- `livy_posttype_setup` *(array)* - Content returned by `Livy\getPossiblePostTypes()` is passed through this filter. This content must be returned as an array of possible post types. If you want to add a post type to the selection options in Composer settings from your theme, use this hook to do so.

- `livy_posttype_load` *(array)* - Content returned by `Livy\getActivePostTypes()` is passed through this filter. Similar to `livy_posttype_setup`, but adding post types to this filter will _bypass_ the selection in Composer settings, and will force the post type to load the Composer. Be careful using this, as it makes it a little more difficult for users to determine what post types Composer is active on (since the types selected in Composer settings will not reflect reality).

- `livy_block_setup` *(array)* - Content returned by `Livy\getPossibleBlockTypes()` is passed through this filter. This is a list of possible block types that is displayed on Composer settings. If you want to add a block type from a theme, hook into this filter to add it to the array of possible block types.

- `livy_block_load` *(array)* - Content returned by `Livy\getActiveBlockTypes()` is passed through this filter. This is a list of active block types that is used by Livy to determine which fields to show in the Composer. By hooking into this filter, a user can "force" a block type to be added. Be careful using this, as it can make it more difficult for users to determine what block types are active (since the types selected in Composer settings will not reflect reality).

- `livy_admin_user_load` *(array)* - Content returned by `Livy\getActiveAdminRoles()` is passed through this, meaning that it could be used to force a change to the roles that recieve access to the settings page. The Administrator role is always in the list of active roles, and you could use this filter to remove it from that list, but doing so is NOT recommended, since you could end up in a situation where no one can access the Composer settings.

- `livy_admin_user_setup` *(array)* - Content returned by `Livy\getPossibleAdminRoles()` is passed through this, which allows you to add roles to the role selection settings if the plugin has not automatically detected them.

- `livy_build_load_blocks` *(array)* - Content passed to the build loop in `Livy\build()` is passed through this filter before the loop begins. This is, essentially, an array of all the meta fields from the post in question. They could be manipulated at this step, but you should only do so with great care, lest you break the loop in some way.

- `livy_build_output` *(string)* - The final output of `Livy\build()` just before it's hashed and written to the actual post. Since this is a string it's not particularly easy to manipulate, but it gives you a final change to touch the content before it's saved.

### Actions

- `livy_run` *(args: Livy object)* - Executed when `Livy\runPlugin()` is run. Useful for setting up things that you want to happen when the plugin runs. 

## Adding or Replacing Blocks in your Theme

The architecture of Livy is such that you can add your own blocks, as well as override the definition or layout of existing blocks.

### Available Blocks

To allow a block to be used, it must be added to the available blocks. To do this, you'll need to hook into the `livy_block_setup` filter. This can be done like so:

```php
function add_new_block( $content ) {
    $content[] = 'new_block';
    return $content;
}

add_filter( 'livy_block_setup', 'add_new_block' );
```

This will introduce the name of your block into the list of available blocks. For it to be displayed, you'll need to enable it on the Composer options menu under "Block Types."

> **Note:** This does *not* cause your block type to appear in the block types list on a post.

You can also *force* your block to always be available (i.e. it can't be turned off under Composer Settings) by using the `livy_block_load` filter instead of `livy_block_setup`.

### Definition

In order for your block to actually appear in the list of block types on a post and store data, you'll need to add a definition file. To do this, put a file in your theme folder with the name `livy-def-[block name].php`. So, if your block is called `new_block`, your definition file would be named `livy-def-new_block.php`. Your definition must follow the standards uses for other defintion files (see the `def-` files in the block folders in `src/blocks`).

> **Note:** If you name your defintion file such that the name collides with one of Livy's built-in blocks (i.e. `text`, `headline`, etc) your definition will *override* the existing one. This is a powerful, but you should be careful using it. It is recommended that you prepend all of your custom block names with some term unique to your theme, to avoid collision.

### Layout

The layout defines how your block will appear. To create a layout for a custom block, add a file to your theme files with the name `live-layout-[block name].php`. So, if your block is called `new_block`, your layout file should be called `livy-layout-new_block.php`. Your layout should follow the standards used for the other layout files (see the `layout-` files in the block folders in `src/blocks`). You can customize the layout however you want, but it is *strongly* recommended that you wrap it in the `__livy__block` class.

> **Note:** If your layout file is named in such as way that it collides with one of Livy's built-in blocks (i.e. `text`, `headline`, etc), it will *override* the existing layout for that block. This allow you to customize how already-defined blocks appear, if you wish to do so. If you don't wish to override blocks, it is recommended that you prepend your block names with a term unique to your theme.

#### Options

##### Preview

Blocks will show a short preview of their content in the top bar when collapsed: The idea is to make it easier to see what a block is when it's collapsed. By default, the Composer looks for the first `input` or `textarea` field it can find in that block, copies the first ~200 characters of text, and inserts them into the top bar. You can customize this by adding the class `preview-text-field` to the field you'd like to use to generate the preview. If this field appears multiple times in your block (i.e., if it's a repeatable element) then the preview will take the first instance of it. For an example implementation, see `def-picture.php`.

##### Hide Label

By default, each field has an associated label. In some cases, however, this is undesirable: For instance if there is only a single field in the block, the label is likely redundant. In this case, just add the class `__livy__admin__hide_label` to the field that should have it's label hidden.


## Capabilities/Roles

At present capabilities are only used to govern who has access to the Composer settings page.

### Capabilities

`livy_settings_access` - If a user has the capability, they are able to access the Composer settings page. The Administrator is automatically granted this role, although this could be overridden through the use of the `livy_admin_user_load`/`livy_admin_user_setup` filters.


## Adding Post Types

Livy will attach itself to some sensible default post types (currently `post` and `page`) but it can be assigned to any post type. To do so, add the following in your theme:

```php
function add_new_post_type ( $content ) {
    $content[] = 'post_type';
    return $content;
}

add_filter('livy_posttype_setup', 'add_new_post_type');
```

This will add the "post_type" post type to the Post Types options in the Composer settings, allowing you to select it.

If, however, you want to *force* display on a post type (i.e. you don't want people to be able to disable the Composer on certain post types) you can use the `livy_posttype_load` filter instead. This will display Livy's Composer on that post type regardless of what the settings in Composer settings say.

-

## Development

If you'd like to fork this or do some other work, clone the repo and then run `composer update && npm install && bower install`. Once this has completed, you're ready to go. To compile CSS and JS, run `gulp`. If you'd like to continually compile CSS and JS as you work, run `gulp watch`.