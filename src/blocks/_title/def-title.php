<?php 

namespace Carbon_Fields\Field;

class Title_Field extends Text_Field
{
    /**
         * Underscore template of this field.
         */
        public function template()
        {
            ?>
            <input id="{{{ id }}}" type="text" name="{{{ name }}}" value="{{ value }}" class="title-text" />
            <?php

        }
}

     $blocks->add_fields($type, array(
            Field::make('title', "{$type}_text", __('Title', 'livy'))
                ->add_class('__livy__admin__hide-label'),
        ));
