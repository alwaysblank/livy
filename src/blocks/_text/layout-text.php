<?php 
    $text = function ($key, $val) {
        $return = new Livy\Cyrus();
        $type = ltrim($val['_type'], '_');
        $return->setEl('div')->setClass('__livy__text')->addContent($val["{$type}_text"])->setClass('__livy__block')->display();
    };
    $text($key, $val);
    unset($text);
