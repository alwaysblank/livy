<?php 

namespace Carbon_Fields\Field;

class ListItem_Field extends Text_Field
{
    /**
         * Underscore template of this field.
         */
        public function template()
        {
            ?>
            <input id="{{{ id }}}" type="text" name="{{{ name }}}" value="{{ value }}" class="listitem-text" />
            <?php

        }
}

    $list_labels = array(
        'plural_name' => __('List Items', 'livy'),
        'singular_name' => __('List Item', 'livy'),
    );

     $blocks->add_fields($type, array(
            Field::make('complex', "{$type}_items", __('List Items', 'livy'))
                ->setup_labels($list_labels)
                ->add_fields(array(
                    Field::make('listitem', "{$type}_item", __('Item', 'livy'))
                        ->add_class('__livy__admin__hide-label')
                        ->add_class('preview-text-field'),
                )),
            Field::make('select', "{$type}_style", __('List Style', 'livy'))
                ->add_options(array(
                    'bullet' => __('Bulleted', 'livy'),
                    'number' => __('Numbers', 'livy'),
                    'none' => __('None', 'livy'),
                ))
                ->help_text(__('Select which type of list you would like this to be.', 'livy')),
        ));
