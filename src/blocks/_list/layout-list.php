<?php 
    $list = function ($key, $val) {
        $return = new Livy\Cyrus();
        $type = ltrim($val['_type'], '_');
        $return->setClass('__livy__list');
        $list_style = $val["{$type}_style"];
        switch ($list_style) {
            case 'bullet':
                $return->setEl('ul');
                break;

            case 'number':
                $return->setEl('ol');
                break;

            case 'none':
                $return->setEl('ul')->setClass('unstyled');
                break;

            default:
                $return->setEl('ul');
                break;
        }
        foreach ($val["{$type}_items"] as $item) :
            $return->openChild()->setEl('li')->setClass('__livy__list__list-item')->addContent($item["{$type}_item"])->closeChild();
        endforeach;
        $return->setClass('__livy__block')->display();

    };
    $list($key, $val);
    unset($list);
