<?php 
    $picture = function ($key, $val) {
        $return = new Livy\Cyrus();

        $type = ltrim($val['_type'], '_');

        $size = 'thumbnail';
        if ($val["{$type}_size"] == 'custom') :
            $size = $val["{$type}_custom_size"];
        else :
            $size = $val["{$type}_size"];
        endif;

        $destination = $val["{$type}_link_type"];
        $image = wp_get_attachment_image($val["{$type}_picture"], $size);
        $image_el = new Livy\Cyrus();
        $image_el->setEl('div')->setClass('__livy__picture__image-wrap');
        if (!$destination) :
            $image_el->addContent($image);
        elseif ($destination) :
            $image_el->setEl('a');
            switch ($destination) {
                case 'internal' :
                    $image_el->setURL(get_permalink($val["{$type}_internal_link"]))->setClass('internal');
                    break;

                case 'external' :
                    $image_el->setURL($val["{$type}_external_link"])->setAttr('target', 'new')->setClass('external');
                    break;

                case 'self' :
                    $image_link = wp_get_attachment_image_src($val["{$type}_picture"], $val["{$type}_self_link"]);
                    $image_el->setURL($image_link[0])->setClass('self');
                    break;

                default:
                    $image_el->setURL(wp_get_attachment_image_src($val["{$type}_picture"], 'large')[0])->setClass('fallback');
                    break;
            }
            $image_el->addContent($image);
        endif;

        $return->setEl('figure')->setClass('__livy__picture')->addContent($image_el);

        if ($val["{$type}_caption"]) :
            $return->openChild()->setEl('figcaption')->addContent($val["{$type}_caption"])->closeChild();
        endif;

        $return->setClass('__livy__block')->display();
    };
    $picture($key, $val);
    unset($picture);
