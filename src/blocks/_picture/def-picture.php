<?php
use Carbon_Fields\Field;
use Coduo\PHPHumanizer\StringHumanizer;

$size_array = array(
            'large' => __('Large', 'livy'),
            'medium' => __('Medium', 'livy'),
            'thumbnail' => __('Small', 'livy'),
            'custom' => __('Custom', 'livy'),
        );

    $custom_array = array('full' => __('Full', 'livy'));
    foreach (get_intermediate_image_sizes() as $value) :
        $custom_array[$value] = StringHumanizer::humanize($value);
    endforeach;

    $page_array = array();
    foreach (get_posts(['post_types' => ['post', 'page']]) as $value) {
        $page_array[$value->ID] = $value->post_title;
    }

    $blocks->add_fields($type, array(
        Field::make('image', "{$type}_picture", __('Photo', 'livy'))
            ->set_width('33.333'),
        Field::make('select', "{$type}_size", __('Size', 'livy'))
            ->add_options($size_array)
            ->help_text(__("Select the size you'd like this image displayed at.", 'livy'))
            ->set_width('33.333'),
        Field::make('select', "{$type}_custom_size", __('Custom Size', 'livy'))
            ->set_conditional_logic(array(
                array(
                    'field' => "{$type}_size",
                    'value' => 'custom',
                    'compare' => '=',
                ),
            ))
            ->add_options($custom_array)
            ->set_width('33.333')
            ->help_text(__('This is a list of <b>all</b> available image sizes. <br><b>Note:</b> If an image is not large enough for the requested size, the next smaller size will be displayed.', 'livy')),
        Field::make('text', "{$type}_caption", __('Caption','livy'))
            ->help_text(__('If no caption is entered, none will be shown.', 'livy'))
            ->add_class('preview-text-field')
            ->set_width('100'),
        Field::make('select', "{$type}_link_type", __('Link', 'livy'))
            ->help_text(__("If you'd like to link this image to something, select that here.", 'livy'))
            ->set_width('20')
            ->add_options(array(
                false => __('None', 'livy'),
                'self' => __('Self', 'livy'),
                'internal' => __('Internal', 'livy'),
                'external' => __('External', 'livy'),
            )),
        Field::make('select', "{$type}_internal_link", __('Internal Link', 'livy'))
            ->add_options($page_array)
            ->set_width('25')
            ->set_conditional_logic(array(
                array(
                    'field' => "{$type}_link_type",
                    'value' => 'internal',
                    'compare' => '=',
                ),
            )),
        Field::make('text', "{$type}_external_link", __('External Link', 'livy'))
            ->set_width('25')
            ->set_conditional_logic(array(
                array(
                    'field' => "{$type}_link_type",
                    'value' => 'external',
                    'compare' => '=',
                ),
            )),
        Field::make('select', "{$type}_self_link", __('Size', 'livy'))
            ->add_options($custom_array)
            ->set_width('25')
            ->help_text(__("Select the size of image you'd like to link to.", 'livy'))
            ->set_conditional_logic(array(
                array(
                    'field' => "{$type}_link_type",
                    'value' => 'self',
                    'compare' => '=',
                ),
            )),
    ));
