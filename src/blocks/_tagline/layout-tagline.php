<?php 
    $tagline = function ($key, $val) {
        $return = new Livy\Cyrus();
        $type = ltrim($val['_type'], '_');
        $return->setEl('h3')->setClass('__livy__tagline')->addContent($val["{$type}_text"])->setClass('__livy__block')->display();
    };
    $tagline($key, $val);
    unset($tagline);
