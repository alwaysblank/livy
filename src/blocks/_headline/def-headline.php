<?php 

namespace Carbon_Fields\Field;

class Headline_Field extends Text_Field
{
    /**
         * Underscore template of this field.
         */
        public function template()
        {
            ?>
            <input id="{{{ id }}}" type="text" name="{{{ name }}}" value="{{ value }}" data-char-limit="75" class="headline-text" />
            <?php

        }
}

     $blocks->add_fields($type, array(
            Field::make('headline', "{$type}_text", __('Headline', 'livy'))
                ->add_class('__livy__admin__hide-label'),
        ));
