<?php 
    $headline = function ($key, $val) {
        $return = new Livy\Cyrus();
        $type = ltrim($val['_type'], '_');
        $return->setEl('h2')->setClass('__livy__headline')->addContent($val["{$type}_text"])->setClass('__livy__block')->display();
    };
    $headline($key, $val);
    unset($headline);
