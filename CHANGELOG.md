## v0.2.0

Cleaned up things.

- Renamed methods and variables to match project standards
- Rearranged files in a more sensible fashion
- Added translation wrapper to all hard-coded text
- Factored out safeString method check to use safeString from hipparchus\pocketknife
- Updated composer dependencies


## v0.1.3

Fixed bug with content getting saved and adjusted settings

- Content is now saved the first time even with the `editor` capability removed
- Livy will now automatically build a list of all available post types it can find for the Composer settings page
    - By default it excludes some WordPress post types that won't work well with the Composer (i.e. nav menus, attachments)
- Users can now customize which users roles can access the Composer settings, in the Composer settings
    - Administrator access to settings is hardcoded, but can be overridden by tying into admin role hooks
    - Livy will automatically build a list of all available user roles it can find (i.e. it should be able to detect custom roles created by other things)
- Slight tweaks to picture block

## v0.1.2

Improvements to admin UX, development workflow, and asset loading.

- Implemented more complete version of block preview functionality introduced in v0.1.1.
- Implemented some options that can be added in defintions to control preview and label behavior
- Admin and frontend assets built separately via gulp
- Admin and frontend assets loaded dynamically for front and back end

## v0.1.1

Updates to documentation, and some usability improvements.

- README.md updates to explain some functionality
- Blocks will now show a snippet of text from their first field (if text or textarea) in the header bar (this feature is unfinished)


## v0.1.0

Initial implementation of core functionality.

- Several basic block types: headline, intro, list, picture, tagline, text, and title
- Implemented system to allow themes to add and override block definitions and layout
- Calls to `the_content` pass through a function that checks content against a hash to determine if it's up to date and recreates it if the test fails
- Gulp-based build system for JS and CSS